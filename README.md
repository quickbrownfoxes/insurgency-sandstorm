## Getting started
```sh
# You should all this inside a folder in your home directory.
# Example: /home/andy/insurgency-sandstorm/
wget -O linuxgsm.sh https://linuxgsm.sh && chmod +x linuxgsm.sh && bash linuxgsm.sh inssserver

# Grab the server files and install any dependencies.
./inssserver install

# Make directory where game-configs should go.
mkdir -p /home/$user/insurgency/serverfiles/Insurgency/Config

```

## Configs

**THIS WILL DELETE THE DEFAULT CONFIG**

Then use commands in linkconfig.sh to symlink the configs in this repo to the configs on the server. You'll need to change the user here.

**chmod 400 the source Game.ini file (not the symlink)** If you don't do this it'll get replaced with an empty one on server start.

Don't edit the ```common.cfg``` or ```_default.cfg``` files in the server-configs. Edit the ```inssserver.cfg``` instead.

Make sure to look through ```inssserver.cfg``` in ```server-configs```

## Enable XP Gain on the server

**XP GAIN WILL NOT WORK ON PASSWORDED SERVERS REGARDLESS OF THE TOKEN**

Login with your steam account here: https://steamcommunity.com/dev/managegameservers

Generate a token for the App ID ```581320```

Copy the ```Login Token``` into a file inside your insurgency folder. 

Edit server-configs/inssserver.cfg to cat the file you specifed. Default: ```/home/vincent/insurgency/gslt.txt```